# API REST definition

**Master:** [![pipeline status](https://gitlab.com/webengproteam/url-shortener/api-rest-definition/badges/master/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/api-rest-definition/commits/master)
**Test:** [![pipeline status](https://gitlab.com/webengproteam/url-shortener/api-rest-definition/badges/test/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/api-rest-definition/commits/test)

OpenAPI definition and client/server stubs for the REST API. This API is responsible of authetication and the creation, modification and deletion of redirections and hooks.

## API docs

Live OpenAPI UI available at [openapi.yaml](https://gitlab.com/webengproteam/url-shortener/api-rest-definition/blob/master/openapi.yaml) (in Gitlab's OpenAPI view mode).

Live documentation available at [https://webengproteam.gitlab.io/url-shortener/api-rest-definition](https://webengproteam.gitlab.io/url-shortener/api-rest-definition).

Even though it is not properly represented at neither of those docs, all the endpoints require to pass a JWT token in the request headers. The exception are the POST methods in /users and /users/login, which are the endpoints that provide tokens. Example:

```bash
curl -X GET "https://gitlab.com/v3/uris/abc123" -H  "accept: application/json" -H  "Authorization: Bearer MyJWTToKeN123"
```

The openAPI UI does this automatically by pressing the green "Authorize" button and adding a valid token.

## Server stubs

- [Python-flask](https://gitlab.com/webengproteam/url-shortener/api-rest-definition/-/jobs/artifacts/master/download?job=deploy-server-python-flask)
- [NodeJS](https://gitlab.com/webengproteam/url-shortener/api-rest-definition/-/jobs/artifacts/master/download?job=deploy-server-nodejs)


## Client stubs

- [Python](https://gitlab.com/webengproteam/url-shortener/api-rest-definition/-/jobs/artifacts/master/download?job=deploy-client-python)
- [Javascript](https://gitlab.com/webengproteam/url-shortener/api-rest-definition/-/jobs/artifacts/master/download?job=deploy-client-javascript)

